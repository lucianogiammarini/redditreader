package ar.edu.unc.famaf.redditreader.backend;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Base64;
import android.util.Log;
import android.util.Pair;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ar.edu.unc.famaf.redditreader.Consts;
import ar.edu.unc.famaf.redditreader.Utils;
import ar.edu.unc.famaf.redditreader.model.Listing;
import ar.edu.unc.famaf.redditreader.model.PostModel;

/**
 * Created by luciano on 29/09/16.
 */
public class Backend {
    private static final String TAG = Backend.class.getSimpleName();
    private static Backend instance = new Backend();
    private static final int POSTS_PER_PAGE = 5;

    public static Backend getInstance() {
        return instance;
    }

    private Backend() {
    }

    private static String getAbsoluteUrl(String relativeUrl) {
        return Consts.BASE_URL + relativeUrl;
    }

    public interface PostsIteratorListener {
        void nextPosts(List<PostModel> posts);
    }

    /**
     * Returns: empty list if it reaches the end, or
     *          null if there is no connection and the db is empty
     * */
    public void getNextPosts(int page, final Context context, final PostsIteratorListener listener) {
        final RedditDBHelper dbHelper = new RedditDBHelper(context);

        boolean connection = Utils.getInstance().isNetworkAvailable(context);
        if (page == 1 && connection) {
            // Si es la primer pagina descargar los posts
            new GetTopPostsTask() {
                @Override
                protected void onPostExecute(List<PostModel> result) {
                    if (result != null && !result.isEmpty()) {
                        dbHelper.insertPosts(result, true);
                        try {
                            result = result.subList(0, POSTS_PER_PAGE);
                        } catch (IndexOutOfBoundsException e) {}
                    }

                    listener.nextPosts(result);
                }
            }.execute();
            return;
        }

        int offset = (page - 1) * POSTS_PER_PAGE;
        List<PostModel> posts = dbHelper.readPosts(POSTS_PER_PAGE, offset);
        if (!connection && posts.isEmpty()) {
            listener.nextPosts(null);
        } else {
            listener.nextPosts(posts);
        }

    }

    private class GetTopPostsTask extends AsyncTask<Void, Void, List<PostModel>> {
        private final String S_URL = "https://www.reddit.com/top/.json?limit=50";

        GetTopPostsTask(){
        }

        @Override
        protected List<PostModel> doInBackground(Void... params) {
            HttpURLConnection conn = null;

            try {
                conn = (HttpURLConnection) new URL(S_URL).openConnection();
                conn.setRequestMethod("GET");
                InputStream is = conn.getInputStream();

                Listing listing = Parser.getInstance().readJsonStream(is);
                return listing.getChildren();
            }
            catch (IOException e) {
                Log.e(TAG, "Downloading TopPosts Failed: " + e.getMessage());
            } finally {
                if (conn != null) {
                    conn.disconnect();
                }
            }
            return null;
        }
    }

    public void getToken(String authCode, final RefreshTokenListener listener) {

        new GetTokenTask() {
            @Override
            protected void onPostExecute(Pair<String, String> result) {
                if (result != null)
                    listener.newToken(result);
                else
                    listener.fail();
            }
        }.execute(authCode);
    }

    public interface RefreshTokenListener {
        /* Returns a Pair<token, refresh_token> */
        void newToken(Pair<String, String> token);
        void fail();
    }

    private class GetTokenTask extends AsyncTask<String, Void, Pair<String, String>> {

        GetTokenTask(){
        }

        @Override
        protected Pair<String, String> doInBackground(String... params) {
            HttpURLConnection conn = null;

            String authCode = params[0];
            String absoluteUrl = getAbsoluteUrl(Consts.TOKEN_URL);

            try {
                conn = (HttpURLConnection) new URL(absoluteUrl).openConnection();
                String userCredentials = Consts.CLIENT_ID + ":" + Consts.CLIENT_SECRET;
                String basicAuth = "Basic " + Base64.encodeToString(userCredentials.getBytes(), Base64.DEFAULT);
                conn.setRequestProperty("Authorization", basicAuth);

                conn.setRequestMethod("POST");
                conn.setDoInput(true);
                conn.setDoOutput(true);

                HashMap<String, String> postDataParams = new HashMap<>();
                postDataParams.put("code", authCode);
                postDataParams.put("grant_type", Consts.GRANT_TYPE);
                postDataParams.put("redirect_uri", Consts.REDIRECT_URI);

                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(os, "UTF-8"));
                writer.write(Utils.getInstance().getPostDataString(postDataParams));

                writer.flush();
                writer.close();
                os.close();

                InputStream is = conn.getInputStream();
                return Parser.getInstance().readToken(is);
            }
            catch (IOException e) {
                Log.e(TAG, "GetToken Failed: " + e.getMessage());
            } finally {
                if (conn != null) {
                    conn.disconnect();
                }
            }
            return null;
        }
    }

    public void revokeToken(String token) {
        new RevokeTokenTask() {}.execute(token);
    }

    private class RevokeTokenTask extends AsyncTask<String, Void, Void> {

        RevokeTokenTask(){
        }

        @Override
        protected Void doInBackground(String... params) {
            HttpURLConnection conn = null;

            String token = params[0];

            String absoluteUrl = getAbsoluteUrl(Consts.REVOKE_TOKEN_URL);

            try {
                conn = (HttpURLConnection) new URL(absoluteUrl).openConnection();
                String userCredentials = Consts.CLIENT_ID + ":" + Consts.CLIENT_SECRET;
                String basicAuth = "Basic " + Base64.encodeToString(userCredentials.getBytes(), Base64.DEFAULT);
                conn.setRequestProperty("Authorization", basicAuth);

                conn.setRequestMethod("POST");
                conn.setDoInput(true);
                conn.setDoOutput(true);

                HashMap<String, String> postDataParams = new HashMap<>();
                postDataParams.put("token", token);
                postDataParams.put("token_type_hint", "access_token");

                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(os, "UTF-8"));
                writer.write(Utils.getInstance().getPostDataString(postDataParams));

                writer.flush();
                writer.close();
                os.close();
                int result = conn.getResponseCode();
                Log.d(TAG, "RevokeToken Response Code: " + result);
            }
            catch (IOException e) {
                Log.e(TAG, "RevokeTokenTask Failed: " + e.getMessage());
            } finally {
                if (conn != null) {
                    conn.disconnect();
                }
            }
            return null;
        }
    }

    /*public void refreshToken(String refresh_token, final RefreshTokenListener listener) {

        new RefreshTokenTask() {
            @Override
            protected void onPostExecute(Pair<String, String> result) {
                if (result != null)
                    listener.newToken(result);
                else
                    listener.fail();
            }
        }.execute(refresh_token);
    }

    private class RefreshTokenTask extends AsyncTask<String, Void, Pair<String, String>> {

        RefreshTokenTask(){
        }

        @Override
        protected Pair<String, String> doInBackground(String... params) {
            HttpURLConnection conn = null;

            String refresh_token = params[0];
            String absoluteUrl = getAbsoluteUrl(Consts.TOKEN_URL);

            try {
                conn = (HttpURLConnection) new URL(absoluteUrl).openConnection();
                String userCredentials = Consts.CLIENT_ID + ":" + Consts.CLIENT_SECRET;
                String basicAuth = "Basic " + Base64.encodeToString(userCredentials.getBytes(), Base64.DEFAULT);
                conn.setRequestProperty("Authorization", basicAuth);

                conn.setRequestMethod("POST");
                conn.setDoInput(true);
                conn.setDoOutput(true);

                HashMap<String, String> postDataParams = new HashMap<>();
                postDataParams.put("grant_type", "refresh_token");
                postDataParams.put("refresh_token", refresh_token);

                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(os, "UTF-8"));
                writer.write(Utils.getInstance().getPostDataString(postDataParams));

                writer.flush();
                writer.close();
                os.close();

                if (conn.getResponseCode() != HttpURLConnection.HTTP_OK) {
                    Log.d(TAG, "http response code is " + conn.getResponseCode());
                    return null;
                }

                InputStream is = conn.getInputStream();
                return Parser.getInstance().readToken(is);
            }
            catch (IOException e) {
                Log.e(TAG, "RefreshToken Failed: " + e.getMessage());
            } finally {
                if (conn != null) {
                    conn.disconnect();
                }
            }
            return null;
        }
    }*/

    public interface InvalidTokenListener {
        void tokenExpired();
    }

    public void votePost(String token, String postId, String dir, final InvalidTokenListener listener) {
        new VotePostTask() {

            @Override
            protected void onPostExecute(Integer result) {
                Log.d(TAG, "Vote Response Code: " + result);
                if (result == HttpURLConnection.HTTP_UNAUTHORIZED) {
                    listener.tokenExpired();
                }
            }

        }.execute(token, postId, dir);
    }

    private class VotePostTask extends AsyncTask<String, Void, Integer> {

        VotePostTask(){
        }

        @Override
        protected Integer doInBackground(String... params) {
            HttpURLConnection conn = null;

            String token = params[0];
            String postId = "t3_" + params[1];
            String dir = params[2];

            String absoluteUrl = "https://oauth.reddit.com/api/vote/";

            try {
                conn = (HttpURLConnection) new URL(absoluteUrl).openConnection();
                conn.setRequestProperty("Authorization", "Bearer " + token);

                conn.setRequestMethod("POST");
                conn.setDoInput(true);
                conn.setDoOutput(true);

                HashMap<String, String> postDataParams = new HashMap<>();
                postDataParams.put("id", postId);
                postDataParams.put("dir", dir);

                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(os, "UTF-8"));
                writer.write(Utils.getInstance().getPostDataString(postDataParams));

                writer.flush();
                writer.close();
                os.close();

                return conn.getResponseCode();
            }
            catch (IOException e) {
                Log.e(TAG, "VotePostTask Failed: " + e.getMessage());
            } finally {
                if (conn != null) {
                    conn.disconnect();
                }
            }
            return null;
        }
    }

}
