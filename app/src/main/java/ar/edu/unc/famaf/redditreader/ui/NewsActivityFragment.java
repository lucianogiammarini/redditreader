package ar.edu.unc.famaf.redditreader.ui;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import ar.edu.unc.famaf.redditreader.R;
import ar.edu.unc.famaf.redditreader.backend.Backend;
import ar.edu.unc.famaf.redditreader.backend.EndlessScrollListener;
import ar.edu.unc.famaf.redditreader.model.PostModel;

/**
 * A placeholder fragment containing a simple view.
 */
public class NewsActivityFragment extends Fragment implements Backend.PostsIteratorListener {
    private static final String TAG = NewsActivityFragment.class.getSimpleName();
    private PostAdapter mPostAdapter;
    private OnPostItemSelectedListener mCallback;

    public NewsActivityFragment() {
    }

    public interface OnPostItemSelectedListener{
        void onPostItemPicked(PostModel post);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_news, container, false);

        List<PostModel> lst = new ArrayList<>();
        mPostAdapter = new PostAdapter(getContext(), R.layout.post_row, lst);
        ListView list = (ListView)view.findViewById(R.id.list);
        list.setOnScrollListener(new EndlessScrollListener() {
            @Override
            public boolean onLoadMore(int page, int totalItemsCount) {
                Backend.getInstance().getNextPosts(page, NewsActivityFragment.this.getActivity(),
                        NewsActivityFragment.this);
                return true;
            }
        });
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                mCallback.onPostItemPicked((PostModel) parent.getItemAtPosition(position));
            }
        });
        list.setAdapter(mPostAdapter);

        Backend.getInstance().getNextPosts(1, getActivity(), this);
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        try {
            mCallback = (OnPostItemSelectedListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement OnPostItemSelectedListener");
        }
    }

    @Override
    public void nextPosts(List<PostModel> posts) {
        if (posts == null) {
            Toast toast = Toast.makeText(getContext(), R.string.check_internet_connection,
                    Toast.LENGTH_LONG);
            toast.show();
        } else if (!posts.isEmpty()) {
            mPostAdapter.appendData(posts);
            mPostAdapter.notifyDataSetChanged();
        }
    }
}
