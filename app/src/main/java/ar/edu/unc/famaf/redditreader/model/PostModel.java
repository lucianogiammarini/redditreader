package ar.edu.unc.famaf.redditreader.model;

import android.graphics.Bitmap;
import android.os.Parcel;
import android.os.Parcelable;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Date;

/**
 * Created by luciano on 28/09/16.
 */
public class PostModel implements Parcelable {
    private static final String POST_HINT_LINK = "link";
    private String mId;
    private Date mCreated;
    private String mTitle;
    private String mSubreddit;
    private int mComments;
    private String mAuthor;
    private URL mThumbnailUrl;
    private Bitmap mThumbnail;
    private int mScore;
    private URL mUrl;
    private String mPostHint;

    public PostModel(){
    }

    protected PostModel(Parcel in) {
        mId = in.readString();
        mCreated = new Date(in.readLong());
        mTitle = in.readString();
        mSubreddit = in.readString();
        mComments = in.readInt();
        mAuthor = in.readString();
        try {
            mThumbnailUrl = new URL(in.readString());
        } catch (MalformedURLException e) {}
        mThumbnail = in.readParcelable(Bitmap.class.getClassLoader());
        mScore = in.readInt();
        try {
            mUrl = new URL(in.readString());
        } catch (MalformedURLException e) {}
        mPostHint = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mId);
        dest.writeLong(mCreated.getTime());
        dest.writeString(mTitle);
        dest.writeString(mSubreddit);
        dest.writeInt(mComments);
        dest.writeString(mAuthor);
        if (mThumbnailUrl != null)
            dest.writeString(mThumbnailUrl.toString());
        else
            dest.writeString("");
        dest.writeParcelable(mThumbnail, flags);
        dest.writeInt(mScore);
        if (mUrl != null)
            dest.writeString(mUrl.toString());
        else
            dest.writeString("");
        dest.writeString(mPostHint);
    }

    public static final Creator<PostModel> CREATOR = new Creator<PostModel>() {
        @Override
        public PostModel createFromParcel(Parcel in) {
            return new PostModel(in);
        }

        @Override
        public PostModel[] newArray(int size) {
            return new PostModel[size];
        }
    };

    public String getId() {
        return mId;
    }

    public void setId(String id) {
        mId = id;
    }

    public String getAuthor() {
        return mAuthor;
    }

    public void setAuthor(String author) {
        mAuthor = author;
    }

    public Date getCreated() {
        return mCreated;
    }

    public void setCreated(Date created) {
        this.mCreated = created;
    }

    public void setCreated(long created) {
        this.mCreated = new Date(created);
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        this.mTitle = title;
    }

    public String getSubreddit() {
        return mSubreddit;
    }

    public void setSubreddit(String subreddit) {
        this.mSubreddit = subreddit;
    }

    public int getComments() {
        return mComments;
    }

    public void setComments(int comments) {
        this.mComments = comments;
    }

    public URL getThumbnailUrl() {
        return mThumbnailUrl;
    }

    public void setThumbnailUrl(URL thumbnailUrl) {
        this.mThumbnailUrl = thumbnailUrl;
    }

    public Bitmap getThumbnail() {
        return mThumbnail;
    }

    public void setThumbnail(Bitmap thumbnail) {
        this.mThumbnail = thumbnail;
    }

    public boolean hasThumbnail() { return this.mThumbnail != null; }

    public int getScore() {
        return mScore;
    }

    public void setScore(int score) {
        this.mScore = score;
    }

    public URL getUrl() {
        return mUrl;
    }

    public void setUrl(URL url) {
        this.mUrl = url;
    }

    public String getPostHint() {
        return mPostHint;
    }

    public void setPostHint(String postHint) {
        this.mPostHint = postHint;
    }

    public boolean isLink() {
        String hint = this.getPostHint();
        return hint != null && hint.equals(POST_HINT_LINK);
    }
}