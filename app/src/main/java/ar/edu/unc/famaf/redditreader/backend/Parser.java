package ar.edu.unc.famaf.redditreader.backend;

import android.util.JsonReader;
import android.util.JsonToken;
import android.util.Log;
import android.util.Pair;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import ar.edu.unc.famaf.redditreader.model.Listing;
import ar.edu.unc.famaf.redditreader.model.PostModel;

/**
 * Created by luciano on 20/10/16.
 */
public class Parser {
    private static Parser instance = new Parser();

    public static Parser getInstance() {
        return instance;
    }

    private Parser() {
    }

    public Listing readJsonStream(InputStream in) throws IOException {
        JsonReader reader = new JsonReader(new InputStreamReader(in, "UTF-8"));
        try {
            reader.beginObject();
            while (reader.hasNext()) {
                String name = reader.nextName();
                if (name.equals("data")) {
                    return readListing(reader);
                } else {
                    reader.skipValue();
                }
            }
            reader.endObject();
        } finally {
            reader.close();
        }
        return null;
    }

    public Listing readListing(JsonReader reader) throws IOException {
        Listing listing = new Listing();

        reader.beginObject();
        while (reader.hasNext()) {
            String name = reader.nextName();
            if (name.equals("after") && reader.peek() != JsonToken.NULL) {
                listing.setAfter(reader.nextString());
            } else if (name.equals("before") && reader.peek() != JsonToken.NULL) {
                listing.setBefore(reader.nextString());
            } else if (name.equals("children") && reader.peek() != JsonToken.NULL) {
                listing.setChildren(readChildrenArray(reader));
            } else {
                reader.skipValue();
            }
        }
        reader.endObject();
        return listing;
    }

    public List<PostModel> readChildrenArray(JsonReader reader) throws IOException {
        List<PostModel> children = new ArrayList<>();
        reader.beginArray();
        while (reader.hasNext()) {
            children.add(readPost(reader));
        }
        reader.endArray();
        return children;
    }

    public PostModel readPost(JsonReader reader) throws IOException {
        PostModel post = new PostModel();
        reader.beginObject();
        while (reader.hasNext()) {
            String _name = reader.nextName();
            if (_name.equals("data")) {
                reader.beginObject();
                while (reader.hasNext()) {
                    String name = reader.nextName();
                    if (name.equals("id")) {
                        post.setId(reader.nextString());
                    } else if (name.equals("author")) {
                        post.setAuthor(reader.nextString());
                    } else if (name.equals("title")) {
                        post.setTitle(reader.nextString());
                    } else if (name.equals("num_comments")) {
                        post.setComments(reader.nextInt());
                    } else if (name.equals("subreddit")) {
                        post.setSubreddit(reader.nextString());
                    }  else if (name.equals("thumbnail") && reader.peek() != JsonToken.NULL) {
                        try {
                            URL url = new URL(reader.nextString());
                            post.setThumbnailUrl(url);
                        } catch (MalformedURLException e) {
                        }
                    } else if (name.equals("created")) {
                        post.setCreated(reader.nextLong());
                    } else if (name.equals("score")) {
                        post.setScore(reader.nextInt());
                    }  else if (name.equals("url") && reader.peek() != JsonToken.NULL) {
                        try {
                            URL url = new URL(reader.nextString());
                            post.setUrl(url);
                        } catch (MalformedURLException e) {}
                    } else if (name.equals("post_hint")) {
                        post.setPostHint(reader.nextString());
                    } else {
                        reader.skipValue();
                    }
                }
                reader.endObject();
            } else {
                reader.skipValue();
            }
        }
        reader.endObject();
        return post;
    }

    public Pair<String, String> readToken(InputStream in) throws IOException {
        String token = null;
        String refresh_token = null;

        JsonReader reader = new JsonReader(new InputStreamReader(in, "UTF-8"));
        try {
            reader.beginObject();
            while (reader.hasNext()) {
                String name = reader.nextName();
                if (name.equals("access_token")) {
                    token = reader.nextString();
                } else if (name.equals("refresh_token")) {
                    refresh_token = reader.nextString();
                }  else if (name.equals("error")) {
                    Log.e("ACCESS_TOKEN", reader.nextString());
                } else {
                    //Log.d("ACCESS_TOKEN", name + ": " + reader.nextString());
                    reader.skipValue();
                }
            }
            reader.endObject();
        } finally {
            reader.close();
        }
        return Pair.create(token, refresh_token);
    }
}
