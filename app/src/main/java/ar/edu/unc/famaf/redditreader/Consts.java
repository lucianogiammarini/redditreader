package ar.edu.unc.famaf.redditreader;

/**
 * Created by luciano on 08/12/16.
 */

public final class Consts  {

    public static final String CLIENT_ID = "0DYOPz5GdsOcEA";
    public static final String CLIENT_SECRET = "";
    public static final String REDIRECT_URI = "http://example.com/redirect_uri";
    public static final String OAUTH_BASE_URL = "https://www.reddit.com/api/v1/authorize.compact";
    public static final String OAUTH_SCOPE = "vote";
    public static final String DURATION = "permanent";
    public static final String BASE_URL = "https://www.reddit.com/api/v1/";
    public static final String GRANT_TYPE = "authorization_code";
    public static final String TOKEN_URL = "access_token";
    public static final String REVOKE_TOKEN_URL = "revoke_token";

    public static final String PREFERENCE_TOKEN = "Token";
    public static final String PREFERENCE_REFRESH_TOKEN = "RefreshToken";
    public static final String PREFERENCE_AUTH_CODE = "Code";

    // PRIVATE //

    /**
     The caller references the constants using <tt>Consts.EMPTY_STRING</tt>,
     and so on. Thus, the caller should be prevented from constructing objects of
     this class, by declaring this private constructor.
     */
    private Consts(){
        //this prevents even the native class from
        //calling this ctor as well :
        throw new AssertionError();
    }
}