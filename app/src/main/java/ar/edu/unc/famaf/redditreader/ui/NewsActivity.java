package ar.edu.unc.famaf.redditreader.ui;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import ar.edu.unc.famaf.redditreader.Consts;
import ar.edu.unc.famaf.redditreader.R;
import ar.edu.unc.famaf.redditreader.Utils;
import ar.edu.unc.famaf.redditreader.backend.Backend;
import ar.edu.unc.famaf.redditreader.model.PostModel;

public class NewsActivity extends AppCompatActivity implements
        NewsActivityFragment.OnPostItemSelectedListener {
    private static final String TAG = NewsActivity.class.getSimpleName();
    public static final String POST = "POST";

    SharedPreferences pref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_news);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        pref = getSharedPreferences("AppPref", MODE_PRIVATE);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_news, menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        String token = pref.getString(Consts.PREFERENCE_TOKEN, null);
        boolean signed_in = token != null;

        menu.findItem(R.id.action_sign_in).setVisible(!signed_in);
        menu.findItem(R.id.action_sign_out).setVisible(signed_in);
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    protected void onResume() {
        super.onResume();
        invalidateOptionsMenu();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_sign_in) {
            if (Utils.getInstance().isNetworkAvailable(this)) {
                Intent intent = new Intent(this, LoginActivity.class);
                startActivity(intent);
            } else {
                Toast.makeText(getApplicationContext(), R.string.check_internet_connection,
                        Toast.LENGTH_LONG).show();
            }
            return true;
        }
        else if (id == R.id.action_sign_out) {
            Backend.getInstance().revokeToken(pref.getString(Consts.PREFERENCE_TOKEN, ""));

            SharedPreferences.Editor edit = pref.edit();
            edit.remove(Consts.PREFERENCE_TOKEN);
            edit.remove(Consts.PREFERENCE_REFRESH_TOKEN);
            edit.apply();

            invalidateOptionsMenu();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onPostItemPicked(PostModel post) {
        Intent intent = new Intent(this, NewsDetailActivity.class);
        intent.putExtra(POST, post);
        startActivity(intent);
    }
}
