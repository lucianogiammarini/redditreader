package ar.edu.unc.famaf.redditreader.ui;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import ar.edu.unc.famaf.redditreader.R;
import ar.edu.unc.famaf.redditreader.backend.RedditDBHelper;
import ar.edu.unc.famaf.redditreader.model.PostModel;
import ar.edu.unc.famaf.redditreader.ui.NewsActivity;

/**
 * Created by luciano on 28/09/16.
 */

public class PostAdapter extends ArrayAdapter<PostModel> {
    private static final String TAG = NewsActivity.class.getSimpleName();
    private List<PostModel> mLstPostModel;
    private Context mContext;

    public PostAdapter(Context context, int resource, List<PostModel> lst) {
        super(context, resource);
        mContext = context;
        mLstPostModel = lst;
    }

    @Override
    public int getCount() {
        return mLstPostModel.size();
    }

    @Override
    public PostModel getItem(int position) {
        return mLstPostModel.get(position);
    }

    @Override
    public boolean isEmpty() {
        return mLstPostModel.isEmpty();
    }

    public void updateData(List<PostModel> lst) {
        mLstPostModel = lst;
    }

    public void appendData(List<PostModel> lst) {
        mLstPostModel.addAll(lst);
    }

    private class PostItemHolder {
        TextView mAuthor;
        TextView mDate;
        TextView mTitle;
        TextView mSubreddit;
        TextView mComments;
        ImageView mThumbnail;
        ProgressBar mProgressBar;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater mInflater = (LayoutInflater) mContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        Bitmap thumbnail;
        final PostItemHolder postItemHolder;

        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.post_row, parent, false);
        }
        if (convertView.getTag() == null){
            postItemHolder = new PostItemHolder();
            postItemHolder.mDate = (TextView) convertView.findViewById(R.id.tv_post_created);
            postItemHolder.mTitle = (TextView) convertView.findViewById(R.id.tv_post_title);
            postItemHolder.mSubreddit = (TextView) convertView.findViewById(R.id.tv_post_subreddit);
            postItemHolder.mComments = (TextView) convertView.findViewById(R.id.tv_post_comments);
            postItemHolder.mAuthor = (TextView) convertView.findViewById(R.id.tv_post_author);
            postItemHolder.mThumbnail = (ImageView) convertView.findViewById(R.id.iv_post_picture);
            postItemHolder.mProgressBar = (ProgressBar) convertView.findViewById(R.id.progressBar);
            convertView.setTag(postItemHolder);
        } else {
            postItemHolder = (PostItemHolder) convertView.getTag();
        }

        final PostModel item = getItem(position);
        if (item == null)
            return convertView;

        Calendar cal = Calendar.getInstance();
        String dateFormat = mContext.getResources().getString(R.string.post_created_format);
        SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
        sdf.setTimeZone(cal.getTimeZone());
        postItemHolder.mDate.setText(sdf.format(item.getCreated()));

        postItemHolder.mTitle.setText(item.getTitle());
        postItemHolder.mAuthor.setText(item.getAuthor());
        postItemHolder.mSubreddit.setText(mContext.getString(R.string.post_subreddit_prefix, String.valueOf(item.getSubreddit())));
        postItemHolder.mComments.setText(mContext.getString(R.string.post_comments_suffix, String.valueOf(item.getComments())));

        if ((thumbnail = item.getThumbnail()) != null){
            postItemHolder.mThumbnail.setImageBitmap(thumbnail);
            postItemHolder.mThumbnail.setVisibility(View.VISIBLE);
            postItemHolder.mProgressBar.setVisibility(View.GONE);
            return convertView;
        }

        URL thumbnailUrl = item.getThumbnailUrl();
        if (thumbnailUrl != null) {
            postItemHolder.mThumbnail.setVisibility(View.VISIBLE);
            postItemHolder.mProgressBar.setVisibility(View.VISIBLE);
            new ImageDownloaderTask(){
                @Override
                protected void onPostExecute(Bitmap result) {
                    postItemHolder.mProgressBar.setVisibility(View.GONE);
                    if (result != null) {
                        postItemHolder.mThumbnail.setImageBitmap(result);
                        item.setThumbnail(result);
                        updatePostThumbnail(item, result);
                    } else {
                        Drawable res = ContextCompat.getDrawable(mContext, R.mipmap.ic_launcher);
                        postItemHolder.mThumbnail.setImageDrawable(res);
                    }
                }
            }.execute(item.getThumbnailUrl());
        } else {
            //Drawable res = ContextCompat.getDrawable(mContext, R.mipmap.ic_launcher);
            //postItemHolder.mThumbnail.setImageDrawable(res);
            postItemHolder.mThumbnail.setVisibility(View.GONE);
            postItemHolder.mProgressBar.setVisibility(View.GONE);
        }
        return convertView;
    }

    private void updatePostThumbnail(final PostModel post, final Bitmap thumbnail) {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                RedditDBHelper dbHelper = new RedditDBHelper(mContext);
                dbHelper.updatePostThumbnail(post.getId(), thumbnail);
                return null;
            }
        }.execute();
    }

    public class ImageDownloaderTask extends AsyncTask<URL, Void, Bitmap> {

        public ImageDownloaderTask() {}

        @Override
        protected Bitmap doInBackground(URL... urls) {
            return download_Image(urls[0]);
        }

        @Nullable
        private Bitmap download_Image(URL url) {
            HttpURLConnection connection = null;

            try {
                connection = (HttpURLConnection) url.openConnection();
                connection.setReadTimeout(5000);
                InputStream is = connection.getInputStream();
                return BitmapFactory.decodeStream(is);
            } catch (SocketTimeoutException e) {
                Log.e(TAG, "SocketTimeout");
            } catch (IOException e) {
                Log.e(TAG, "Downloading Image Failed");
            } finally {
                if (connection != null) {
                    connection.disconnect();
                }
            }
            return null;
        }
    }
}
