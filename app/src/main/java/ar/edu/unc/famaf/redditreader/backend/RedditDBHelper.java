package ar.edu.unc.famaf.redditreader.backend;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.graphics.Bitmap;
import android.util.Log;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import ar.edu.unc.famaf.redditreader.model.PostModel;

/**
 * Created by luciano on 27/10/16.
 */

public class RedditDBHelper extends SQLiteOpenHelper {
    private static final String TAG = RedditDBHelper.class.getSimpleName();
    public static final int DB_VERSION = 1;
    private static final String DATABASE_NAME = "redditreader_db.db";
    public static final String POST_TABLE = "post";
    public static final String POST_TABLE_ID = "_id";
    public static final String POST_TABLE_POST_ID = "post_id";
    public static final String POST_TABLE_AUTHOR = "author";
    public static final String POST_TABLE_TITLE = "title";
    public static final String POST_TABLE_SUBREDDIT = "subreddit";
    public static final String POST_TABLE_COMMENTS = "comments";
    public static final String POST_TABLE_THUMBNAIL_URL = "thumbnail_url";
    public static final String POST_TABLE_THUMBNAIL = "thumbnail";
    public static final String POST_TABLE_CREATED = "created";
    public static final String POST_TABLE_SCORE = "score";
    public static final String POST_TABLE_URL = "url";
    public static final String POST_TABLE_POST_HINT = "post_hint";

    public RedditDBHelper(Context context) {
        super(context, DATABASE_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String createSentence = "create table " + POST_TABLE + " ("
                + POST_TABLE_ID + " integer primary key autoincrement,"
                + POST_TABLE_POST_ID + " text not null unique,"
                + POST_TABLE_AUTHOR + " text not null,"
                + POST_TABLE_TITLE + " text not null,"
                + POST_TABLE_SUBREDDIT + " text not null,"
                + POST_TABLE_COMMENTS + " integer not null,"
                + POST_TABLE_THUMBNAIL_URL + " text,"
                + POST_TABLE_THUMBNAIL + " blob,"
                + POST_TABLE_CREATED + " integer not null,"
                + POST_TABLE_SCORE + " integer not null,"
                + POST_TABLE_URL + " text,"
                + POST_TABLE_POST_HINT + " text"
                + ");";
        db.execSQL(createSentence);
        Log.d("DB", "Database Created");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.d("DB", "Database Updated");
        db.execSQL("DROP TABLE IF EXISTS " + POST_TABLE);
        this.onCreate(db);
    }

    public void insertPost(PostModel post) {
        List<PostModel> lst = new ArrayList<>();
        lst.add(post);
        insertPosts(lst);
    }

    public void insertPosts(List<PostModel> lst) {
        this.insertPosts(lst, false);
    }

    public void insertPosts(List<PostModel> lst, boolean clear) {
        if (clear)
            clearPosts();

        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();
        try {
            for (PostModel post : lst) {
                ContentValues values = new ContentValues();
                values.put(POST_TABLE_POST_ID, post.getId());
                values.put(POST_TABLE_AUTHOR, post.getAuthor());
                values.put(POST_TABLE_TITLE, post.getTitle());
                values.put(POST_TABLE_SUBREDDIT, post.getSubreddit());
                values.put(POST_TABLE_COMMENTS, post.getComments());
                values.put(POST_TABLE_CREATED, post.getCreated().getTime());
                Bitmap thumbnail = post.getThumbnail();
                if (thumbnail != null)
                    values.put(POST_TABLE_THUMBNAIL, DbBitmapUtility.getBytes(post.getThumbnail()));
                URL thumbnailUrl = post.getThumbnailUrl();
                if (thumbnailUrl != null)
                    values.put(POST_TABLE_THUMBNAIL_URL, thumbnailUrl.toString());

                values.put(POST_TABLE_SCORE, post.getScore());
                URL url = post.getUrl();
                if (url != null)
                    values.put(POST_TABLE_URL, url.toString());
                values.put(POST_TABLE_POST_HINT, post.getPostHint());

                db.insert(POST_TABLE, null, values);
            }
            db.setTransactionSuccessful();
        } catch (Exception e) {
            Log.e(TAG, "Error inserting Post: " + e.getMessage());
        } finally {
            db.endTransaction();
            db.close();
        }
    }

    public List<PostModel> readPosts(int limit, int offset) {
        List<PostModel> lst = new ArrayList<>();
        String selectQuery = "SELECT  * FROM " + POST_TABLE + " LIMIT " + offset + "," + limit;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        try {
            if (cursor.moveToFirst()) {
                do {
                    PostModel post = new PostModel();
                    post.setId(cursor.getString(1));
                    post.setAuthor(cursor.getString(2));
                    post.setTitle(cursor.getString(3));
                    post.setSubreddit(cursor.getString(4));
                    post.setComments(cursor.getInt(5));
                    try {
                        post.setThumbnailUrl(new URL(cursor.getString(6)));
                    } catch (MalformedURLException e) {}
                    try {
                        post.setThumbnail(DbBitmapUtility.getImage(cursor.getBlob(7)));
                    } catch (NullPointerException e) {}
                    post.setCreated(cursor.getInt(8));
                    post.setScore(cursor.getInt(9));
                    try {
                        post.setUrl(new URL(cursor.getString(10)));
                    } catch (MalformedURLException e) {}
                    post.setPostHint(cursor.getString(11));

                    lst.add(post);
                } while (cursor.moveToNext());
            }
        } finally {
            db.close();
        }
        return lst;
    }

    public void clearPosts() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(POST_TABLE, null, null);
        db.close();
    }

    public void updatePostThumbnail(String id, Bitmap thumbnail){
        if (thumbnail == null)
            return;

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(POST_TABLE_THUMBNAIL, DbBitmapUtility.getBytes(thumbnail));
        db.update(POST_TABLE, values, POST_TABLE_POST_ID + "=?", new String[]{id});
        db.close();
    }
}
