package ar.edu.unc.famaf.redditreader.ui;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import ar.edu.unc.famaf.redditreader.Consts;
import ar.edu.unc.famaf.redditreader.R;
import ar.edu.unc.famaf.redditreader.backend.Backend;
import ar.edu.unc.famaf.redditreader.model.PostModel;


public class NewsDetailActivityFragment extends Fragment implements Backend.InvalidTokenListener {

    private PostModel post;
    String token;

    public NewsDetailActivityFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_news_detail_activity, container, false);

        Intent intent = getActivity().getIntent();
        post = intent.getParcelableExtra(NewsActivity.POST);

        SharedPreferences pref = getActivity().getSharedPreferences("AppPref", Context.MODE_PRIVATE);
        token = pref.getString(Consts.PREFERENCE_TOKEN, null);

        ImageButton voteDownIB = (ImageButton) view.findViewById(R.id.ib_vote_down);
        voteDownIB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                v.startAnimation(AnimationUtils.loadAnimation(getActivity(),
                        R.anim.image_button_click));
                votePost("-1");
            }
        });

        ImageButton voteUpIB = (ImageButton) view.findViewById(R.id.ib_vote_up);
        voteUpIB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.startAnimation(AnimationUtils.loadAnimation(getActivity(),
                        R.anim.image_button_click));
                votePost("1");
            }
        });

        TextView urlTV = (TextView) view.findViewById(R.id.tv_postdetail_url);
        urlTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(NewsDetailActivityFragment.this.getActivity(), 
                        WebViewActivity.class);
                intent.putExtra(WebViewActivity.URL, post.getUrl().toString());
                startActivity(intent);
            }
        });

        setPost(view, post);
        
        return view;
    }

    private void votePost(String dir) {
        if (token == null) {
            Toast.makeText(getActivity(), R.string.must_be_logged_in,
                    Toast.LENGTH_LONG).show();
            return;
        }
        Backend.getInstance().votePost(token, post.getId(), dir, this);
    }

    private void setPost(View view, PostModel post) {
        TextView titleTV = (TextView) view.findViewById(R.id.tv_postdetail_title);
        titleTV.setText(post.getTitle());

        TextView authorTV = (TextView) view.findViewById(R.id.tv_postdetail_author);
        authorTV.setText(post.getAuthor());

        TextView commentsTV = (TextView) view.findViewById(R.id.tv_postdetail_comments);
        commentsTV.setText(getResources().getString(R.string.post_comments_suffix,
                String.valueOf(post.getComments())));

        Calendar cal = Calendar.getInstance();
        String dateFormat = getResources().getString(R.string.post_created_format);
        SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
        sdf.setTimeZone(cal.getTimeZone());

        TextView createdTV = (TextView) view.findViewById(R.id.tv_postdetail_created);
        createdTV.setText(sdf.format(post.getCreated()));

        TextView subredditTV = (TextView) view.findViewById(R.id.tv_postdetail_subreddit);
        subredditTV.setText(getResources().getString(R.string.post_subreddit_prefix, String.valueOf(post.getSubreddit())));

        TextView urlTV = (TextView) view.findViewById(R.id.tv_postdetail_url);
        if (post.isLink()){
            urlTV.setText(post.getUrl().toString());
        } else {
            urlTV.setVisibility(View.GONE);
        }

        ImageView thumbnailIV = (ImageView) view.findViewById(R.id.iv_postdetail_picture);
        if (post.hasThumbnail()) {
            thumbnailIV.setImageBitmap(post.getThumbnail());
        } else {
            thumbnailIV.setVisibility(View.GONE);
        }

        TextView scoreTV = (TextView) view.findViewById(R.id.tv_postdetail_score);
        scoreTV.setText(String.valueOf(post.getScore()));
    }

    @Override
    public void tokenExpired() {
        Toast.makeText(getActivity(), R.string.token_expired,
                Toast.LENGTH_LONG).show();
        SharedPreferences pref = getActivity().getSharedPreferences("AppPref", Context.MODE_PRIVATE);
        SharedPreferences.Editor edit = pref.edit();
        edit.remove(Consts.PREFERENCE_TOKEN);
        edit.remove(Consts.PREFERENCE_REFRESH_TOKEN);
        edit.apply();
    }
}
