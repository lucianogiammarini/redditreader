package ar.edu.unc.famaf.redditreader.ui;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.util.Pair;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import java.util.UUID;

import ar.edu.unc.famaf.redditreader.Consts;
import ar.edu.unc.famaf.redditreader.R;
import ar.edu.unc.famaf.redditreader.backend.Backend;

/**
 * A login screen that offers login via email/password.
 */
public class LoginActivity extends AppCompatActivity implements Backend.RefreshTokenListener {

    private static final String state = UUID.randomUUID().toString();

    private static final String OAUTH_URL = Consts.OAUTH_BASE_URL + "?client_id=" + Consts.CLIENT_ID +
            "&response_type=code&state=" + state + "&redirect_uri=" + Consts.REDIRECT_URI +
            "&duration=" + Consts.DURATION + "&scope=" + Consts.OAUTH_SCOPE;

    private String authCode;
    SharedPreferences pref;


    private ProgressBar mProgressView;
    private WebView mWebView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        mProgressView = (ProgressBar) findViewById(R.id.login_progress);
        mWebView = (WebView) findViewById(R.id.webview_auth);

        pref = getSharedPreferences("AppPref", MODE_PRIVATE);
        /*String authCode = pref.getString(Consts.PREFERENCE_AUTH_CODE, null);

        if (authCode == null) {
            getAuthCode();
        } else {
            getToken(authCode);
        }*/
        getAuthCode();
    }

    private void getAuthCode() {
        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.loadUrl(OAUTH_URL);

        mWebView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);

                if (url.contains("?code=") || url.contains("&code=")) {

                    Uri uri = Uri.parse(url);
                    authCode = uri.getQueryParameter("code");
                    SharedPreferences.Editor edit = pref.edit();
                    edit.putString(Consts.PREFERENCE_AUTH_CODE, authCode);
                    edit.apply();

                    Log.d("code", authCode);
                    showProgress(true);

                    getToken(authCode);
                } else if (url.contains("error=access_denied")) {
                    Log.i("", "ACCESS_DENIED_HERE");
                    setResult(Activity.RESULT_CANCELED);
                    finish();
                }
            }
        });
    }

    private void getToken(String authCode) {
        try {
            Backend.getInstance().getToken(authCode, LoginActivity.this);
        } catch (Exception e) {
            e.printStackTrace();
            setResult(Activity.RESULT_CANCELED);
            finish();
        }
    }

    @Override
    public void newToken(Pair<String, String> tokens) {
        SharedPreferences.Editor edit = pref.edit();
        edit.putString(Consts.PREFERENCE_TOKEN, tokens.first);
        if (tokens.second != null)
            edit.putString(Consts.PREFERENCE_REFRESH_TOKEN, tokens.second);
        edit.apply();

        Log.d("Token", tokens.first);

        setResult(Activity.RESULT_OK);
        finish();
    }

    @Override
    public void fail(){
        setResult(Activity.RESULT_CANCELED);
        finish();
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mWebView.setVisibility(show ? View.GONE : View.VISIBLE);
            mWebView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mWebView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mWebView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }
}

