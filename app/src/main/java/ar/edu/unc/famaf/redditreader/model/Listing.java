package ar.edu.unc.famaf.redditreader.model;

import java.util.List;

/**
 * Created by luciano on 20/10/16.
 */

public class Listing {
    private String mBefore;
    private String mAfter;
    private List<PostModel> mChildren;

    public Listing() {
    }

    public String getBefore() {
        return mBefore;
    }

    public void setBefore(String before) {
        this.mBefore = before;
    }

    public String getAfter() {
        return mAfter;
    }

    public void setAfter(String after) {
        this.mAfter = after;
    }

    public List<PostModel> getChildren() {
        return mChildren;
    }

    public void setChildren(List<PostModel> children) {
        this.mChildren = children;
    }
}
